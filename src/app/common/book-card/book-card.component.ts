import { Component, Input } from '@angular/core';
import { ShelfService } from 'src/app/shelf/services/shelf.service';

@Component({
  selector: 'book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.css']
})
export class BookCardComponent {
  @Input() book: Object;

  readStatus:Array<Object> = [
    {id: 0, name: "CURRENTLY READING"},
    {id: 1, name: "WANT TO READ"},
    {id: 2, name: "READ"},
    {id: -1, name: "NONE"},
  ];

  constructor(private shelfService: ShelfService){}

  updateStatus(id: Number){
    this.shelfService.updateStatus(this.book, id);
  }
}
