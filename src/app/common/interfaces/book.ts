export interface Book {
    isbn: String;
    title: String;
    subtitle: String;
    author: String;
    imagePath: String;
    readStatus?: Number;
}
