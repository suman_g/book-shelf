import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ShelfService } from '../shelf/services/shelf.service';
import { ReadStatus } from '../common/enums/read-status.enum';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {
  readStatus: typeof ReadStatus = ReadStatus;

  searchFormControl = new FormControl('');
  formCtrlSub: Subscription;
  searchText: String = '';

  searchResults: Object[];

  constructor(private shelfService: ShelfService) { }

  ngOnInit() {
    this.formCtrlSub = this.searchFormControl.valueChanges
      .pipe(debounceTime(1000))
      .subscribe(newValue => {
        this.searchText = newValue;

        if(this.searchText === '') {
          this.searchResults = [];
          return;
        }

        this.searchResults = this.shelfService.getBooks().filter(eachBook => {
          return eachBook["readStatus"] === -1 && eachBook["title"].toLowerCase().indexOf(newValue) !== -1;
        });
      });
  }

  ngOnDestroy(): void {
    this.formCtrlSub.unsubscribe();
  }
}
