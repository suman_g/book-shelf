import { Injectable } from '@angular/core';
import { ReadStatus } from '../../common/enums/read-status.enum';

@Injectable({
  providedIn: 'root'
})
export class ShelfService {
  private books: Object[];
  readStatus: typeof ReadStatus = ReadStatus;

  currentlyReadingBooks: Object[];
  wantToReadBooks: Object[];
  readBooks: Object[];

  constructor() {
    this.init();
    this.filterBooks();
  }

  init(){
    this.books = [
      {
        isbn: 1,
        title: "The Alchemist",
        subtitle: "The Alchemist",
        author: "Paulo Coelho",
        imagePath: "assets/images/alchemist.jpg",
        readStatus: this.readStatus.WANT_TO_READ        
      },
      {
        isbn: 2,
        title: "The Alchemist Revised",
        subtitle: "The Alchemist",
        author: "Paulo Coelho",
        imagePath: "assets/images/alchemist.jpg",
        readStatus: this.readStatus.WANT_TO_READ
      },
      {
        isbn: 3,
        title: "Fear",
        subtitle: "By Osho",
        author: "Osho",
        imagePath: "assets/images/fear.jpg",
        readStatus: this.readStatus.CURRENTLY_READING
      },
      {
        isbn: 4,
        title: "To kill a mocking bird",
        subtitle: "to kill a mocking bird",
        author: "Harper Lee",
        imagePath: "assets/images/to kill a mocking bird.jpg",
        readStatus: this.readStatus.READ
      },
      {
        isbn: 5,
        title: "The Alchemist Original",
        subtitle: "The Alchemist",
        author: "Paulo Coelho",
        imagePath: "assets/images/alchemist.jpg",
        readStatus: this.readStatus.WANT_TO_READ
      },
      {
        isbn: 6,
        title: "The Alchemist",
        subtitle: "The Alchemist",
        author: "Paulo Coelho",
        imagePath: "assets/images/dummy.jpg",
        readStatus: this.readStatus.NONE        
      },
      {
        isbn: 7,
        title: "The Alchemist Revised",
        subtitle: "The Alchemist",
        author: "Paulo Coelho",
        imagePath: "assets/images/dummy.jpg",
        readStatus: this.readStatus.NONE
      },
      {
        isbn: 8,
        title: "Fear",
        subtitle: "By Osho",
        author: "Osho",
        imagePath: "assets/images/dummy.jpg",
        readStatus: this.readStatus.NONE
      },
      {
        isbn: 9,
        title: "To kill a mocking bird",
        subtitle: "to kill a mocking bird",
        author: "Harper Lee",
        imagePath: "assets/images/dummy.jpg",
        readStatus: this.readStatus.NONE
      },
      {
        isbn: 10,
        title: "The Alchemist Original",
        subtitle: "The Alchemist",
        author: "Paulo Coelho",
        imagePath: "assets/images/dummy.jpg",
        readStatus: this.readStatus.NONE
      }
    ];
  }

  filterBooks(){
    this.currentlyReadingBooks = [];
    this.wantToReadBooks = [];
    this.readBooks = [];
    
    this.books.forEach(book => {
      switch(book["readStatus"]){
        case this.readStatus.CURRENTLY_READING: this.currentlyReadingBooks.push(book); break;
        case this.readStatus.WANT_TO_READ: this.wantToReadBooks.push(book); break;
        case this.readStatus.READ: this.readBooks.push(book); break;
        default: break;
      }
    });

    console.log("currentlyReadingBooks");    
    console.table(this.currentlyReadingBooks);
    
    console.log("wantToReadBooks");    
    console.table(this.wantToReadBooks);

    console.log("readBooks");    
    console.table(this.readBooks);
  }  

  getBooks(){
     return this.books;
  }

  updateStatus(bookParam, status){
      let index = this.books.findIndex(book => book['isbn'] === bookParam['isbn']);
      this.books.splice(index, 1);

      this.books.push(Object.assign({}, bookParam, {'readStatus':  status}));

      this.filterBooks();
  }
}
