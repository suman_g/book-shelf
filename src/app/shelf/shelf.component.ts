import { Component } from '@angular/core';
import { ShelfService } from './services/shelf.service';

@Component({
  selector: 'app-shelf',
  templateUrl: './shelf.component.html',
  styleUrls: ['./shelf.component.css']
})
export class ShelfComponent {
  constructor(private shelfService: ShelfService) {}
}
